import src.goobiHandler.goobiHandler as goobi
import src.goobiHandler.goobiHelper as goobiHelper
import src.invenio.invenioRest as invenio
import json
from dotenv import load_dotenv
import argparse
import copy
import re

# Load environment variables and configuration
load_dotenv()
# Access config.json
with open('config.json', 'r') as myfile:
    data=myfile.read()
config = json.loads(data)

# Erstelle den ArgumentParser
parser = argparse.ArgumentParser(description='Upload a Publication from Goobi to Zenodo', usage="python3 main.py --vorgangId")
# Füge das Argument 'shoulder' hinzu
parser.add_argument('--vorgangId', '-id', required=True, help='Goobi-Vorgang-ID, dient zum Auslesen und Schreiben der Metadatan', )
# Parse die Argumente
args = parser.parse_args()
# Zugriff auf das Argument 'vorgangId'
print(f"Empfangene Vorgang-ID: {args.vorgangId}")
if args.vorgangId:
    vorgangId = args.vorgangId
else:
    vorgangId = ""

# Script retrieves a Goobi Process ID, fetch the meta*.xml file
metadataPath = config["metadataDir"]+f"/{vorgangId}/meta.xml"
modsXml = goobi.parseMetadata(metadataPath)
#Check structMap@TYPE=LOGICAL which Struct-Types exists. 
structMap = goobi.analyzeStructMap(modsXml, config)
print(structMap)

# based on DocStruct and on Metadata-Mapping given in config.json prepare Invenio Draft Record
for dmd in [item for item in structMap if item['zenodoUpload']]:
    print(dmd["dmdid"]+" : "+dmd["type"])
    logId = re.sub("DMD","",dmd["dmdid"])
    zenodo = invenio.Invenio()
    record = zenodo.recordSchema
    #print(record)
    #parse Metadata for dmdlog
    
    #fetch list of metadata from config and parse them from the dmdlog-xml
    mds = (config["metadataMapping"][dmd["type"]])
    for md in [item for item in config["metadataMapping"][dmd["type"]] if item.get("ugh")]:
        mdInvenio = copy.deepcopy(md["invenio"])

        if md.get("position") and md["position"] == "top":
            dmdid = "DMDLOG_0001"
        else:
            dmdid = dmd["dmdid"]
        
        mdValue = goobiHelper.getNodesMetadata(modsXml, dmdid, md["ugh"])

        if mdValue != []:
            #print(f"{mdInvenio} gets {mdValue[0].text}")
            zenodo.set_mdSubDict(mdInvenio, mdValue[0].text)
            #print(mdInvenio)
            record["metadata"] = zenodo.update_Metadata(record["metadata"], mdInvenio)

        #break
    #print(record)

    for md in [item for item in config["metadataMapping"][dmd["type"]] if item.get("value")]:
        #print(f"set vfix value {md['invenio']}")
        record["metadata"] = zenodo.update_Metadata(record["metadata"], md["invenio"])
    
    for md in [item for item in config["metadataMapping"][dmd["type"]] if item.get("type")]:
        if md["type"].lower() == "license":
            #print("add license information")
            licenseValue = goobiHelper.setLicense(modsXml,dmd["dmdid"])
            record["metadata"] = zenodo.update_Metadata(record["metadata"], licenseValue)
        if md["type"].lower() == "author":
            print("add creators field from ")
            persons = goobiHelper.getPersonData(modsXml,dmd["dmdid"],"Author")
            if persons != None:
                for person in persons:
                    #print(person)
                    mdValue = zenodo.setPersonOrOrg(name=person["displayName"],splitChar=", ", persIdScheme=person["authorityID"], persId=person["authorityValue"])
                    #print(mdValue)
                    record["metadata"]["creators"].append(mdValue)
        if md["type"].lower() == "article-ark":
            print ("fetch first page number for granular article-ark")
            xpathStr = f".//mets:div[@ID=//mets:smLink[@xlink:from=//mets:div[@DMDID='{dmd['dmdid']}']/@ID][1]/@xlink:to]/@ORDER"
            print(xpathStr)
            firstPage = goobi.findMetadata(modsXml, xpathStr=xpathStr)
            ark = goobiHelper.getNodesMetadata(modsXml, dmdid, "ARK")
            print(f"{ark[0].text}/{firstPage[0]}")
        
        if md["type"].lower() == "article-pages":
            print("fetch orderlabel for first and last page of article")
            xpathStr = f".//mets:div[@ID=//mets:smLink[@xlink:from=//mets:div[@DMDID='{dmd['dmdid']}']/@ID][1]/@xlink:to]/@ORDERLABEL"
            firstPage = goobi.findMetadata(modsXml, xpathStr=xpathStr)
            xpathStr = f".//mets:div[@ID=//mets:smLink[@xlink:from=//mets:div[@DMDID='{dmd['dmdid']}']/@ID][last()]/@xlink:to]/@ORDERLABEL"
            print(xpathStr)
            lastPage = goobi.findMetadata(modsXml, xpathStr=xpathStr)
            print(f"{firstPage[0]}-{lastPage[0]}")

    print(record)

    #prepare draft-record

# fetch the necessary pdf-file and upload it
# https://zentralgut.ch/api/v1/records/{CatalogIDDigital}/pdf/?usePdfSource=false
# e.g. Monograph (12703): https://zentralgut.ch/api/v1/records/991171885987505501/pdf/?usePdfSource=false
# https://zentralgut.ch/api/v1/records/{CatalogIDDigital}/sections/{LOG_ID}/pdf/?usePdfSource=false
# e.g. Article :https://zentralgut.ch/api/v1/records/991171879706305501_2023016/sections/LOG_0006/pdf/?usePdfSource=false


# publish zenodo file

# write Zenodo-DOI to meta.xml