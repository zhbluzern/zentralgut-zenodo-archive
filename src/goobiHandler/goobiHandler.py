import lxml.etree as ET
from datetime import datetime
import json


def parseMetadata(filename):
    #mytree = ET.parse(f'metadata/{processId}/meta.xml',parser=ET.XMLParser(remove_blank_text=True))
    mytree = ET.parse(filename, parser=ET.XMLParser(remove_blank_text=True))
    mytree = mytree.getroot()
    return mytree

def findMetadata(mytree, attributeName="", xpathStr = ""):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/', 'xlink':'http://www.w3.org/1999/xlink' }
    ET.register_namespace=namespaces
    if xpathStr == "":
        xpathStr = f".//goobi:metadata[@name='{attributeName}']/text()"
    #print(xpathStr)
    e = mytree.xpath(xpathStr, namespaces=namespaces)
    #print(e)
    return e

def getPersonsAsString(mytree):
    persons = findMetadata(mytree, xpathStr="(//goobi:goobi)[1]/goobi:metadata[@type='person']/goobi:displayName | (//goobi:goobi)[1]/goobi:metadata[@type='person']/@name")
    whoString = ""
    whoStringCounter = 1
    whoStringDet = ""
    for person in persons:
        try:
            whoStringDet = (person.text)+ whoStringDet
        except:
            whoStringDet = f" ({person})"

        if whoStringCounter % 2 == 0:  
            if whoString != "":
                whoString = whoString + "; "
            whoString = whoString + whoStringDet
            whoStringDet = ""
        whoStringCounter += 1
    return whoString

    
def addMetadata(mytree, value, attributeName):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/' }
    ET.register_namespace=namespaces
    xpathFindStr='.//mods:extension/goobi:goobi'
    e = mytree.find(xpathFindStr, namespaces)
    metadata='{http://meta.goobi.org/v1.5.1/}metadata'
    newMetadata = ET.SubElement(e, metadata)

    newMetadata.text = value
    newMetadata.set("name", attributeName)

    print(f"new insert: add {value} for MODS {attributeName}")
    return mytree

def writeNewMetaFile(processId, mytree, filePath):
    mytree = ET.ElementTree(mytree)
    mytree.write(f'{filePath}{processId}/meta.xml',encoding="UTF-8", xml_declaration=True, pretty_print=True)

def backupMetaFile(processId, mytree, filePath):
    mytree = ET.ElementTree(mytree)
    currentDateStr = datetime.utcnow().strftime('%Y-%m-%d-%H%M%S%f')[:-3]
    mytree.write(f'{filePath}{processId}/meta.xml.{currentDateStr}', encoding="UTF-8", xml_declaration=True, pretty_print=True)

# Method analyzeStructMap() selects all logical elements from mets:structMap and 
# compares the TYPE with the config.json for all those elements, which are defined for Zenodo-Upload
# returns a list of type, dmdID and boolean 'zenodoUpload' for further processing
def analyzeStructMap(mytree, configData):
    structMap = findMetadata(mytree, xpathStr=".//mets:structMap[@TYPE='LOGICAL']//mets:div")
    resultSet = []
    for div in structMap:
        if div.get('DMDID'):
            resultDet = {}
            #print(div.get('TYPE')+" "+div.get('DMDID'))
            resultDet["dmdid"] = div.get('DMDID')
            resultDet["type"] = div.get('TYPE')
            if configData["metadataMapping"].get(div.get("TYPE")):
                resultDet["zenodoUpload"] = True
            else:
                resultDet["zenodoUpload"] = False
            resultSet.append(resultDet)
    return resultSet

if __name__ == "__main__":
    with open('config.json', 'r') as myfile:
        data=myfile.read()
    config = json.loads(data)

    modsTree = parseMetadata("metadata/12502/meta.xml")
    structMap = analyzeStructMap(modsTree, config)
    print(structMap)

    print("========================================")

    modsTree = parseMetadata("metadata/12703/meta.xml")
    structMap = analyzeStructMap(modsTree, config)
    print(structMap)