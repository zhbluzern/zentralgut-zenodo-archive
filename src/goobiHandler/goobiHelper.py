import src.goobiHandler.goobiHandler as goobi
import lxml.etree as ET

def setLicense(modsXml, dmdid):
    #mdValue = goobi.findMetadata(modsXml, xpathStr=f".//mets:dmdSec[@ID='DMDLOG_0001']//goobi:metadata[@name='UseAndReproductionLicense']")
    mdValue = getNodesMetadata(modsXml, dmdid, ugh="UseAndReproductionLicense")
    license = {"rights":[{"title": "", "link":"", "description":""}]}

    if mdValue[0].text == "reserved":
        license["rights"][0]["title"] = "In Copyright 1.0"
        license["rights"][0]["link"] = "https://rightsstatements.org/vocab/InC/1.0/"

    return license

def getPersonData(modsXml, dmdid, ugh):
    mdValue = getNodesMetadata(modsXml, dmdid, ugh)
    returnList = []
    if mdValue != []:
        for author in mdValue:
            resultDet={"displayName":"", "authorityValue": "", "authorityID":""}
            resultDet["displayName"] = goobi.findMetadata(author, xpathStr=".//goobi:displayName")[0].text
            authorityValue  = goobi.findMetadata(author, xpathStr=".//goobi:authorityValue")
            authorityId = goobi.findMetadata(author, xpathStr=".//goobi:authorityID")
            if authorityValue != []:
                resultDet["authorityValue"] = authorityValue[0].text
            if authorityId != []:
                resultDet["authorityID"] = authorityId[0].text
            returnList.append(resultDet)
    if returnList != []:
        return returnList
    else:
        return None

def getNodesMetadata(modsXml, dmdid, ugh):
    #print(f"set metadata from ugh {md['ugh']} in {dmdid}")
    xpathStr=f".//mets:dmdSec[@ID='{dmdid}']//goobi:metadata[@name='{ugh}']"
    #print(xpathStr)
    mdValue = goobi.findMetadata(modsXml, xpathStr=xpathStr)
    
    if mdValue == []:
        # Bei leerem Ergebnis Fallback auf DMDLOG_0001 (zB ARK)
        mdValue = goobi.findMetadata(modsXml, xpathStr=f".//mets:dmdSec[@ID='DMDLOG_0001']//goobi:metadata[@name='{ugh}']")
    
    return mdValue