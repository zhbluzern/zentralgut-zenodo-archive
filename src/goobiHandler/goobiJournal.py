import requests
import os
from dotenv import load_dotenv

load_dotenv()

# Replace these variables with your actual username and password
token = os.getenv("apiToken")
apiUrl = os.getenv("goobiApiUrl")

def getJournal(processId, apiToken):
    result = requests.get(url=f"{apiUrl}process/{processId}/journal", headers={"accept":"*/*", "token": apiToken})
    print(result.json())

def writeJournal(processId, apiToken, journalMessage, journalType="info"):
    journalData = { "type": journalType, "message": journalMessage}
    result = requests.post(url=f"{apiUrl}process/{processId}/journal", headers={"accept":"*/*", "token": apiToken, "Content-Type" : "application/json"}, json=journalData)
    print(result.text)

if __name__ == '__main__':
  #getJournal("11344", token)
  writeJournal("11344", token, journalMessage="Das ist ein Test-Info-Journal-Eintrag via REST-API", journalType="info")